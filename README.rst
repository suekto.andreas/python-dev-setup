Python Dev Setup
================
Sample project on setting up automated development environment of Python in
Visual Studio Code

Dependency
----------
1. python 2.7
2. `pip install vitualenv`
3. Visual Studio Code 
4. VSCode Plugin : https://github.com/ryanluker/vscode-coverage-gutters
5. VSCode Plugin : https://github.com/emeraldwalk/vscode-runonsave

Setup
-----
1. Ensure all dependencies are installed
2. open the directory as the root in VSCode
3. initialize the virtualenv

    .. code-block::

        $ virtualenv venv
        $ . venv/bin/activate
        $ pip install autopep8
        $ pip install pylint
        $ pip install coverage
        $ pip install sphinx
        $ pip install sphinx_rtd_theme

Usage
-------
1. Activate the Watch mode of Coverage Gutter Plugin from Ryan Luker
    (https://github.com/ryanluker/vscode-coverage-gutters)
2. Recommendation: Open up the Output of Run On Save Plugin from Emerald Walk
    (https://github.com/emeraldwalk/vscode-runonsave) to see the output of unittest
    or coverage report.
3. Build the document `. tools/build-doc.sh python_dev_setup`

Note
----
The .vscode is deliberately being uploaded for the purpose of showing how it was
setup. For real development especially in a team work, it might be preferable to
gitignore .vscode

Reference
---------
* `Blog-Automate Python Dev in VSCode`_

.. _Blog-Automate Python Dev in VSCode: https://medium.com/@suekto.andreas/setting-up-python-dev-in-vs-code-e84f01c1f64b/