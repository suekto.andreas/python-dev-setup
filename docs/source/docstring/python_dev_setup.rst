python\_dev\_setup package
==========================

Submodules
----------

python\_dev\_setup.multiply module
----------------------------------

.. automodule:: python_dev_setup.multiply
    :members:
    :undoc-members:
    :show-inheritance:

python\_dev\_setup.multiply\_test module
----------------------------------------

.. automodule:: python_dev_setup.multiply_test
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: python_dev_setup
    :members:
    :undoc-members:
    :show-inheritance:
